import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import 'rxjs/add/operator/filter';

import { SettingsComponent } from '../components/settings/settings.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'full-layout.component.html'
})
export class FullLayoutComponent implements OnInit {
  
  disabled: boolean = false;
  status: { isopen: boolean } = { isopen: false };
  showDashboardMenu: boolean;
  @ViewChild(SettingsComponent) readonly settings: SettingsComponent;

  constructor(public router: Router, public route: ActivatedRoute) {
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe(() => {
        let currentRoute = this.route.root;
        while (currentRoute.children[0])
          currentRoute = currentRoute.children[0];
        let showDashboardMenu = currentRoute.snapshot.data['showDashboardMenu'];
        if (showDashboardMenu)
          this.showDashboardMenu = true;
        else
          this.showDashboardMenu = false;
    });
  }

  ngOnInit() { }

  toggled(open: boolean): void {
    console.log('Dropdown is now: ', open);
  }

  toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

  onLogout() {
    this.router.navigate(['/']);
  }

  showModal() {
    this.settings.showModal();
  }

}
