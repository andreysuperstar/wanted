export class Follower {
  name: string;
  image: string;
  gender: string;
  follower: boolean = false;
}