export class User {
  id: number;
  type: string = 'private';
  forename?: string;
  surname?: string;
  image?: string;
  gender?: string;
  location?: string;
  bio?: string;
  followed?: boolean;

  name() {
    return this.forename + 555;
  }
}