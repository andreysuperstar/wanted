import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// import {CognitoCallback, UserLoginService} from '../../../services/cognito.service';

@Component({
  templateUrl: 'password.component.html'
})
// export class PasswordComponent implements CognitoCallback {
export class PasswordComponent {
  
  email: string;
  errorMessage: string;

  // constructor(public router: Router, public route: ActivatedRoute, public userService: UserLoginService) {
  constructor(public router: Router, public route: ActivatedRoute) {
    this.errorMessage = null;
  }

  ngOnInit() {
    this.errorMessage = null;
  }

  onNext() {
    if (this.email == null || this.email == '') {
      this.errorMessage = 'All fields are required';
      return;
    }
    this.errorMessage = null;
    this.router.navigate(['../password2'], { relativeTo: this.route, queryParams: { email: this.email }, skipLocationChange: true });
    // this.userService.forgotPassword(this.email, this);
  }

  cognitoCallback(message: string, result: any) {
    if (message != null && result != null) {
      this.router.navigate(['password', this.email]);
    } else {
      this.errorMessage = message;
    }
  }
}

@Component({
  templateUrl: './password2.component.html'
})
// export class Password2Component implements CognitoCallback, OnInit, OnDestroy {
export class Password2Component implements OnInit, OnDestroy {
  verificationCode: string;
  email: string;
  password: string;
  code: number;
  errorMessage: string;
  private sub: any;

  // constructor(public router: Router, public route: ActivatedRoute, public userService: UserLoginService) {
  constructor(public router: Router, public route: ActivatedRoute) {
    console.log('email from the url: ' + this.email);
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.email = params['email'];

    });
    this.errorMessage = null;
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  onNext() {
    if (this.code == null || this.password == null || this.password == '') {
      this.errorMessage = 'All fields are required';
      return;
    }
    this.errorMessage = null;
    // this.userService.confirmNewPassword(this.email, this.verificationCode, this.password, this);
  }

  cognitoCallback(message: string) {
    if (message != null) {
      this.errorMessage = message;
      console.log('result: ' + this.errorMessage);
    } else {
      this.router.navigate(['/'], { relativeTo: this.route });
    }
  }

}
