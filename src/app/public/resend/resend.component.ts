import { Component, OnInit } from '@angular/core';
// import {CognitoCallback, UserRegistrationService} from '../../../services/cognito.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: 'resend.component.html'
})
export class ResendComponent implements OnInit {

  email: string;
  errorMessage: string;

  // constructor(public registrationService: UserRegistrationService, public router: Router) {
  constructor(public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {
    this.errorMessage = null;
  }

  onResend() {
    if (this.email == null || this.email == '') {
      this.errorMessage = 'All fields are required';
      return;
    }
    this.errorMessage = null;
    // this.registrationService.resendCode(this.email, this);
  }

  cognitoCallback(error: any, result: any) {
    if (error != null) {
      this.errorMessage = 'Something went wrong&hellip; please try again';
    } else {
      this.router.navigate(['../confirm', this.email], { relativeTo: this.route });
    }
  }
  
}