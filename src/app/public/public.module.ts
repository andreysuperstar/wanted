import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { PublicRoutingModule } from './public.routing';
import { LoginComponent } from './login/login.component';
import { PasswordComponent } from './password/password.component';
import { Password2Component } from './password/password.component';
import { RegisterComponent } from './register/register.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { ResendComponent } from './resend/resend.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PublicRoutingModule
  ],
  declarations: [
    LoginComponent,
    PasswordComponent,
    Password2Component,
    RegisterComponent,
    ConfirmComponent,
    ResendComponent
  ]
})
export class PublicModule { }
