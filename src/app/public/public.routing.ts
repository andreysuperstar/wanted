import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { PasswordComponent, Password2Component } from './password/password.component';
import { RegisterComponent } from './register/register.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { ResendComponent } from './resend/resend.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: LoginComponent },
      { path: 'password', component: PasswordComponent },
      { path: 'password2', component: Password2Component },
      { path: 'register', component: RegisterComponent },
      { path: 'confirm', component: ConfirmComponent },
      { path: 'resend', component: ResendComponent },
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class PublicRoutingModule {}
