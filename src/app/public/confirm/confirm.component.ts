import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// import { UserRegistrationService, UserLoginService, LoggedInCallback } from '../../../services/cognito.service';

@Component({
  templateUrl: 'confirm.component.html'
})
// export class LogoutComponent implements LoggedInCallback {
export class LogoutComponent {

  // constructor(public router: Router, public userService: UserLoginService) {
  constructor(public router: Router) {
    // this.userService.isAuthenticated(this)
  }

  isLoggedIn(message: string, isLoggedIn: boolean) {
    if (isLoggedIn) {
      // this.userService.logout();
      this.router.navigate(['public/login']);
    }

    this.router.navigate(['public/login']);
  }
}

@Component({
  templateUrl: 'confirm.component.html'
})
export class ConfirmComponent implements OnInit, OnDestroy {
  code: number;
  email: string;
  errorMessage: string;
  private sub: any;

  // constructor(public regService: UserRegistrationService, public router: Router, public route: ActivatedRoute) { }
  constructor(public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.email = params['username'];

    });

    this.errorMessage = null;
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  onConfirm() {
    if (this.code == null) {
      this.errorMessage = 'All fields are required';
      return;
    }
    this.errorMessage = null;
    // this.regService.confirmRegistration(this.email, this.confirmationCode, this);
  }

  cognitoCallback(message: string, result: any) {
    if (message != null) {
      this.errorMessage = message;
      console.log('message: ' + this.errorMessage);
    } else {
      //move to the next step
      console.log('Moving to securehome');
      // this.configs.curUser = result.user;
      this.router.navigate(['/dashboard']);
    }
  }
  
}
