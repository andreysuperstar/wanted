import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// import { CognitoCallback, UserLoginService, LoggedInCallback} from '../../../services/cognito.service';
// import { DynamoDBService } from '../../../services/ddb.service';

@Component({
  templateUrl: 'login.component.html',
})
// export class LoginComponent implements CognitoCallback, LoggedInCallback, OnInit {
export class LoginComponent implements OnInit {

  username: string;
  email: string;
  password: string;
  errorMessage: string;

  // constructor(public router: Router, public ddb: DynamoDBService, public userService: UserLoginService) {
  constructor(public router: Router, public route: ActivatedRoute ) {}

  ngOnInit() {
    this.errorMessage = null;
    // console.log('Checking if the user is already authenticated. If so, then redirect to the secure site');
    // this.userService.isAuthenticated(this);
  }

  onLogin() {
    if (this.username == null || this.username == '' || this.password == null || this.password == '') {
      this.errorMessage = 'All fields are required';
      return;
    }
    this.errorMessage = null;
    // this.userService.authenticate(this.email, this.password, this);
    this.router.navigate(['../secure/feed'], { relativeTo: this.route });
  }

  cognitoCallback(message: string, result: any) {
    if (message != null) {
      this.errorMessage = message;
      console.log('result: ' + this.errorMessage);
      if (this.errorMessage === 'User is not confirmed.') {
        console.log('redirecting');
        this.router.navigate(['/confirm', this.email]);
      }
    } else {
      // this.ddb.writeLogEntry('login');
      this.router.navigate(['/dashboard']);
    }
  }

  isLoggedIn(message: string, isLoggedIn: boolean) {
    if (isLoggedIn) {
      this.router.navigate(['/dashboard']);
    }
  }
  
}