import { Component } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
// import {UserRegistrationService, CognitoCallback} from '../../../services/cognito.service';

export class RegistrationUser {
  name: string;
  email: string;
  password: string;
  type: string;
}

@Component({
  templateUrl: 'register.component.html'
})
export class RegisterComponent {
  
  registrationUser: RegistrationUser;
  repeat: string;
  errorMessage: string;

  // constructor(public userRegistration: UserRegistrationService, router: Router) {
  constructor(public router: Router, public route: ActivatedRoute) {
    this.router = router;
    this.onInit();
  }

  onInit() {
    this.registrationUser = new RegistrationUser();
    this.errorMessage = null;
  }

  onRegister() {
    if (this.registrationUser.name == null || this.registrationUser.name == '' || this.registrationUser.email == null || this.registrationUser.email == '' || this.registrationUser.password == null || this.registrationUser.password == '' || this.repeat == null || this.repeat == '') {
      this.errorMessage = 'All fields are required';
      return;
    }
    this.errorMessage = null;
    // this.userRegistration.register(this.registrationUser, this);
    this.router.navigate(['../confirm'], { relativeTo: this.route });
  }

  cognitoCallback(message: string, result: any) {
    if (message != null) { //error
      this.errorMessage = message;
      console.log("result: " + this.errorMessage);
    } else { //success
      //move to the next step
      console.log("redirecting");
      this.router.navigate(['../confirm'], { relativeTo: this.route });
    }
  }

}
