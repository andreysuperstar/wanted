import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'cards',
  templateUrl: 'cards.component.html'
})
export class CardsComponent implements OnInit {

  @Input() cards: Object[];
  hasCards: boolean;

  constructor() { }

  ngOnInit() {
    this.hasCards = this.cards.length == 0 ? false : true;
  }
  
}