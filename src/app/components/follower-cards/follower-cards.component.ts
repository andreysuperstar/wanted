import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'follower-cards',
  templateUrl: 'follower-cards.component.html',
  styleUrls: ['follower-cards.component.scss']
})
export class FollowerCardsComponent implements OnInit {

  @Input() followers: Object[];

  constructor() { }

  ngOnInit() { }
  
}