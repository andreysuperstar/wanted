import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ng2-bootstrap/modal/modal.component';

@Component({
  selector: 'settings',
  templateUrl: 'settings.component.html',
  styleUrls: ['settings.component.scss']
})
export class SettingsComponent implements OnInit, AfterViewInit {

  @ViewChild('settingsModal') settingsModal: ModalDirective;
  type: string;
  websiteFollower: boolean;
  websitePost: boolean;
  websiteComment: boolean;
  emailFollower: boolean;
  emailPost: boolean;
  emailComment: boolean;

  constructor() {
    this.websiteFollower = true;
    this.websitePost = true;
    this.emailComment = true;
    this.type = 'private';
    
  }

  ngOnInit() {}

  ngAfterViewInit() {
    // this.showModal();
  }

  onSaveSettings() {
    this.settingsModal.hide();
  }

  showModal() {
    this.settingsModal.show();
  }

}
