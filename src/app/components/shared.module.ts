import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardsComponent } from '../components/cards/cards.component';
import { FollowerCardsComponent } from '../components/follower-cards/follower-cards.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    CardsComponent,
    FollowerCardsComponent
  ],
  exports: [
    CardsComponent,
    FollowerCardsComponent
  ]
})
export class SharedModule { }
