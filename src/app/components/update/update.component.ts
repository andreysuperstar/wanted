import { Component, Input, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ng2-bootstrap/modal/modal.component';

import { User } from '../../models/user.model';

@Component({
  selector: 'update',
  templateUrl: 'update.component.html',
  styleUrls: ['update.component.scss']
})
export class UpdateComponent implements OnInit, AfterViewInit {

  @ViewChild('updateModal') updateModal: ModalDirective;
  @Input() user: User;
  password: string;
  confirm: string;

  ngOnInit() {}

  ngAfterViewInit() {
    // this.showModal();
  }

  onSaveProfile() {
    this.updateModal.hide();
  }

  showModal() {
    this.updateModal.show();
  }

}
