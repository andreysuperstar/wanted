import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SecureRoutingModule } from './secure.routing';

@NgModule({
  imports: [
    CommonModule,
    SecureRoutingModule
  ],
  declarations: [
  ]
})
export class SecureModule { }
