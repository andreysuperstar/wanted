import { Component, OnInit, ViewChild } from '@angular/core';

import { User } from '../../../models/user.model';

import { DataService } from '../../../services/data/data.service';

import { UpdateComponent } from '../../../components/update/update.component';


@Component({
  templateUrl: 'profile.component.html',
  styleUrls: ['profile.component.scss']
})
export class ProfileComponent implements OnInit {

  id: number;
  user: User;
  image: string;
  items: Object[];
  @ViewChild(UpdateComponent) readonly update: UpdateComponent;
  
  constructor(public data: DataService) {
    this.id = 9;
    this.image = 'assets/img/avatars/1.jpg';
    this.items = Array(5);
    this.getUser(this.id);
  }

  ngOnInit() {}

  getUser(id) {
    this.data.getUserById(this.id)
      .subscribe(
        response => this.user = response,
        error => console.log(`Error getUsers() (profile/profile.component.ts): ${<any>error}`)
      );
  }

  showModal() {
    this.update.showModal();
  }
  
}