import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../components/shared.module';

import { ModalModule } from 'ng2-bootstrap/modal';

import { ProfileRoutingModule } from './profile.routing';
import { ProfileComponent } from './profile/profile.component';
import { FollowedComponent } from './followed/followed.component';
import { FollowingComponent } from './following/following.component';
import { UpdateComponent } from '../../components/update/update.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ProfileRoutingModule,
    ModalModule
  ],
  declarations: [
    ProfileComponent,
    FollowedComponent,
    FollowingComponent,
    UpdateComponent
  ]
})
export class ProfileModule { }
