import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileComponent } from './profile/profile.component';
import { FollowedComponent } from './followed/followed.component';
import { FollowingComponent } from './following/following.component';

export const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: ProfileComponent, data: { title: 'Profile' } },
      { path: 'followers', component: FollowedComponent, data: { title: 'Followers' } },
      { path: 'following', component: FollowingComponent, data: { title: 'Following' } }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class ProfileRoutingModule {}
