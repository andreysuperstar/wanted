import { Component, OnInit } from '@angular/core';

import { User } from '../../../models/user.model';

import { DataService } from '../../../services/data/data.service';


@Component({
  templateUrl: 'followed.component.html'
})
export class FollowedComponent implements OnInit {
  
  items: User[];

  constructor(public data: DataService) {}

  ngOnInit() {
    this.getFollowers();
  }

  getFollowers() {
    this.data.getFollowed()
      .subscribe(
        response => this.items = response,
        error => console.log(`Error getFollowers() (feed/followers.component.ts): ${<any>error}`)
      );
  }

}