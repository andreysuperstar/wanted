import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'feed',
    children: [
      {
        path: '',
        loadChildren: './feed/feed.module#FeedModule'
      },
    ]
  },
  {
    path: 'profile',
    children: [
      {
        path: '',
        loadChildren: './profile/profile.module#ProfileModule'
      },
    ]
  },
  {
    path: 'dashboard',
    children: [
      {
        path: '',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class SecureRoutingModule {}
