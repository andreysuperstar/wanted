import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: 'private.component.html'
})
export class PrivateComponent implements OnInit {
  
  items: Object[];

  constructor() {
    this.items = Array(0);
  }

  ngOnInit() { }

}