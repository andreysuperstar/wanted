import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: 'later.component.html'
})
export class LaterComponent implements OnInit {
  
  items: Object[];

  constructor() {
    this.items = Array(1);
  }

  ngOnInit() { }

}