import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: 'home.component.html'
})
export class HomeComponent implements OnInit {
  
  items: Object[];
  
  constructor() {
    this.items = Array(25);
  }

  ngOnInit() { }

}