import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../components/shared.module';

import { FeedRoutingModule } from './feed.routing';
import { HomeComponent } from './home/home.component';
import { FavoritesComponent } from './favorites/favorites.component';
import { PrivateComponent } from './private/private.component';
import { LaterComponent } from './later/later.component';
import { FollowersComponent } from './followers/followers.component';

import { DataService } from '../../services/data/data.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FeedRoutingModule
  ],
  declarations: [
    FavoritesComponent,
    PrivateComponent,
    LaterComponent,
    FollowersComponent,
    HomeComponent
  ],
  providers: [
    DataService
  ]
})
export class FeedModule { }
