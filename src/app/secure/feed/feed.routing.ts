import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { FavoritesComponent } from './favorites/favorites.component';
import { PrivateComponent } from './private/private.component';
import { LaterComponent } from './later/later.component';
import { FollowersComponent } from './followers/followers.component';

export const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: HomeComponent, data: { title: 'Home' } },
      { path: 'favorites', component: FavoritesComponent, data: { title: 'Favorites' } },
      { path: 'private', component: PrivateComponent, data: { title: 'Private Box' } },
      { path: 'later', component: LaterComponent, data: { title: 'Read Later' } },
      { path: 'followers', component: FollowersComponent, data: { title: 'Followers' } },
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class FeedRoutingModule {}
