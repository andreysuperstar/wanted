import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: 'favorites.component.html'
})
export class FavoritesComponent implements OnInit {
  
  items: Object[];

  constructor() {
    this.items = Array(7);
  }

  ngOnInit() { }
  
}