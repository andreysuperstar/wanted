import { Component, OnInit } from '@angular/core';

import { User } from '../../../models/user.model';

import { DataService } from '../../../services/data/data.service';


@Component({
  templateUrl: 'followers.component.html'
})
export class FollowersComponent implements OnInit {
  
  items: User[];

  constructor(public data: DataService) {}

  ngOnInit() {
    this.getFollowers();
  }

  getFollowers() {
    this.data.getFollowers()
      .subscribe(
        response => this.items = response,
        error => console.log(`Error getFollowers() (feed/followers.component.ts): ${<any>error}`)
      );
  }

}