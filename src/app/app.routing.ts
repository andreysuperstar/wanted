import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SimpleLayoutComponent } from './layouts/simple-layout.component';
import { FullLayoutComponent } from './layouts/full-layout.component';

import { NotFoundComponent } from './components/404/404.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'public',
    pathMatch: 'full',
  },
  {
    path: 'public',
    component: SimpleLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: './public/public.module#PublicModule'
      }
    ]
  },
  {
    path: 'secure',
    component: FullLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: './secure/secure.module#SecureModule'
      }
    ]
  },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
