import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { User } from '../../models/user.model';

@Injectable()
export class DataService {

  constructor(private http: Http) { }

  getUsers() {
    return this.http.get('assets/data/users.json')
      .map(response => response.json())
      .map(response => response.users as User[]);
  }

  getUserById(id: number) {
    return this.http.get('assets/data/users.json')
      .map(response => response.json())
      .map(
        response => response.users
          .find(user => user.id == id)
      );
  }

  getFollowers() {
    return this.http.get('assets/data/users.json')
      .map(response => response.json())
      .map(response => response.users as User[]);
  }

  getFollowing() {
    return this.http.get('assets/data/users.json')
      .map(response => response.json())
      .map(response => response.users as User[])
      .map(response => response.slice(0, 7));
  }

  getFollowed() {
    return this.http.get('assets/data/users.json')
      .map(response => response.json())
      .map(response => response.users as User[])
      .map(response => response.slice(8, 10));
  }

}
