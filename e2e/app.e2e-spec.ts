import { WantedPage } from './app.po';

describe('wanted App', () => {
  let page: WantedPage;

  beforeEach(() => {
    page = new WantedPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
